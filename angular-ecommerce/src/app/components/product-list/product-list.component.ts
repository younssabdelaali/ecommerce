import { Component, OnInit } from '@angular/core';
import { PoductService } from 'src/app/services/poduct.service';
import { CarteService } from 'src/app/services/carte.service';

import { Product } from 'src/app/common/Product';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/common/cart-item';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list-grid.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[]= [];
  currentCategoryId: number=1;
  previousCategoryId: number=1;
  previouskeyword: string = null;

  currentCategoryName: string;
  searchMode : boolean=false;
  thePageNumber : number = 1 ;
  thePageSize : number = 5;
  theTotalElements : number = 0;

  constructor(private poductService : PoductService ,
                private route: ActivatedRoute,
                private cartService : CarteService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
    this.listProducts();
    });


  }




  
  listProducts(){
    this.searchMode= this.route.snapshot.paramMap.has('keyword')
    if (this.searchMode) {
      this.handleSearchProducts();
    }else{
    this.handleListProducts();
  }
}
  handleSearchProducts() {
    const thekeyword : string = this.route.snapshot.paramMap.get('keyword');

//if we have a different keyword than previous
//then set thepageNumber to 1
if(this.previouskeyword != thekeyword){
  this.thePageNumber = 1 ;
}
this.previouskeyword=thekeyword
console.log(`the key word is =${thekeyword}, thePageNumber=${this.thePageNumber}`)


/*     now search for products using keyword
 */    this.poductService.searchProdcutListPaginate(this.thePageNumber - 1,this.thePageSize,thekeyword).subscribe(
    this.processResult());
  }

  handleListProducts(){
      /* chek if id parameter is avaibale */
  const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id')

  if (hasCategoryId) {
/* get the id param string convert string to number usinng the "+" symbole
 */ 
this.currentCategoryId= +this.route.snapshot.paramMap.get('id');
this.currentCategoryName = this.route.snapshot.paramMap.get('name')
}else{
  // not category id available ... default category id =1
  this.currentCategoryId=1;
  this.currentCategoryName='Books';
}

//check if we have a different category than previous
//note: angular will reuse a component if it is currently being viewed

//if we have a different category id than previous
// then set thePageNumber back to 1
if(this.previousCategoryId != this.currentCategoryId){
  this.thePageNumber = 1 ;
}
this.previousCategoryId=this.currentCategoryId;

console.log(`currentCategoryId=${this.currentCategoryId}, thePageNumber=${this.previousCategoryId}`)

//now get the products for the given categoty id
    this.poductService.getProdcutListPaginate(this.thePageNumber - 1,this.thePageSize,this.currentCategoryId).subscribe(
    this.processResult());
  }
  processResult(){
    return data => {
      this.products= data._embedded.products;
      this.thePageNumber= data.page.number + 1;
      this.thePageSize=data.page.size;
      this.theTotalElements=data.page.totalElements;
    };
  }


  updatePageSize(pageNumber:number){
    this.thePageNumber=1;
    this.thePageSize= pageNumber ;
    this.listProducts();
  }
addToCart(theProduct: Product){
console.log(`Addingto cart : ${theProduct.name},${theProduct.unitPrice}`)
const theCartItem = new CartItem(theProduct);
this.cartService.addToCart(theCartItem);
}
}

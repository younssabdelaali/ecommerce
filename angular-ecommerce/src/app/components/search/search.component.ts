import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/common/Product';
import { PoductService } from 'src/app/services/poduct.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
products: Product[];
  constructor(private router : Router) { }

  ngOnInit(): void {
  }
doSearch(value: string){
 
  this.router.navigateByUrl(`/search/${value}`)
}

}

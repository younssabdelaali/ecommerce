import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/common/cart-item';
import { Product } from 'src/app/common/Product';
import { CarteService } from 'src/app/services/carte.service';
import { PoductService } from 'src/app/services/poduct.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product: Product= new Product();

  constructor(private route : ActivatedRoute,
              private productService: PoductService,
              private cartService : CarteService) { }

  
  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.handleProductDEtails();
    });
  }
  handleProductDEtails() {
/*     + pour converite
 */    const theProductId : number = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(theProductId).subscribe(
      data => { 
        this.product=data;
      }
    )
  }

  addToCart(){
    console.log(`Addingto cart : ${this.product.name},${this.product.unitPrice}`)
    const theCartItem = new CartItem(this.product);
    this.cartService.addToCart(theCartItem);
    }
}

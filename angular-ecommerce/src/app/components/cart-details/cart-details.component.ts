import { Component, OnInit } from '@angular/core';
import { CartItem } from 'src/app/common/cart-item';
import { CarteService } from 'src/app/services/carte.service';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.css']
})
export class CartDetailsComponent implements OnInit {
 
  cartItems : CartItem[]=[];
  totalPrice : number = 0;
  totalQuantity:   number =0 ;
  constructor(private cartService : CarteService) { }


  ngOnInit(): void {
    this.listCartDetails();
  }
  listCartDetails() {

    this.cartItems= this.cartService.cartItems;
    this.cartService.totalPrice.subscribe(
      data => this.totalPrice =data
    );
    this.cartService.totalQuantity.subscribe(
      data => this.totalQuantity =data
    );
    //compute cart total price and total quantity
    this.cartService.computeCartTotals();
  }

}

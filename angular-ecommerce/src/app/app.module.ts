import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { PoductService } from './services/poduct.service';
import { AppComponent } from './app.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { Routes, RouterModule } from '@angular/router';
import { ProductCategoryMenuComponent } from './components/product-category-menu/product-category-menu.component';
import { SearchComponent } from './components/search/search.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CartStatusComponent } from './components/cart-status/cart-status.component';
import { CartDetailsComponent } from './components/cart-details/cart-details.component';


const routes: Routes = [
  {path :'search/:keyword', component: ProductListComponent},
  {path :'category/:id/:name', component: ProductListComponent},
  {path :'products/:id', component: ProductDetailsComponent},
  {path :'category', component: ProductListComponent},
  {path :'products', component: ProductListComponent},
  {path :'cart-details', component: CartDetailsComponent},
  {path :'',redirectTo: '/products' , pathMatch: 'full'},
  {path :'**',redirectTo: '/products' , pathMatch: 'full'},

];
@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductCategoryMenuComponent,
    SearchComponent,
    ProductDetailsComponent,
    CartStatusComponent,
    CartDetailsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [PoductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
